package com.happen.ems_flowable.entity;

import com.baomidou.mybatisplus.annotation.TableLogic;
import com.baomidou.mybatisplus.annotation.TableName;
import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;
import java.util.Date;

/**
 * @program: ems_flowable
 * @description: 员工
 * @author: happen96
 * @create: 2021-04-26 19:14
 **/
@Data
@AllArgsConstructor
@NoArgsConstructor
@TableName("ems_employee")
public class Employee implements Serializable {
    private Long id;
    private String name;
    private String email;
    private String phone;
    private String sex;
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss",timezone="Asia/Shanghai")
    private Date birthday;
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss",timezone="Asia/Shanghai")
    private Date entryDate;
    @TableLogic
    private Integer deleted;
}
