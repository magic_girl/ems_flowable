package com.happen.ems_flowable;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
@MapperScan("com.happen.ems_flowable.mapper")
public class EmsFlowableApplication {

    public static void main(String[] args) {
        SpringApplication.run(EmsFlowableApplication.class, args);
    }

}
