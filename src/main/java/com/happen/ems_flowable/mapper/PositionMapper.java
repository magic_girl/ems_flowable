package com.happen.ems_flowable.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.happen.ems_flowable.entity.Position;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;

/**
 * @program: ems_flowable
 * @description:
 * @author: happen96
 * @create: 2021-04-27 11:02
 **/
@Mapper
public interface PositionMapper extends BaseMapper<Position> {
    List<Position> list();
}
