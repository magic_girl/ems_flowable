package com.happen.ems_flowable.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.happen.ems_flowable.entity.Employee;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;

/**
 * @program: ems_flowable
 * @description:
 * @author: happen96
 * @create: 2021-04-26 19:31
 **/
@Mapper
public interface EmployeeMapper extends BaseMapper<Employee> {
    List<Employee> list();
}
