package com.happen.ems_flowable.service;

import com.happen.ems_flowable.entity.Employee;

import java.util.List;

/**
 * @program: ems_flowable
 * @description:
 * @author: happen96
 * @create: 2021-04-26 19:45
 **/
public interface EmployeeService {
    List<Employee> list();

    Employee findById(Long id);

    Integer add(Employee employee);

    Integer update(Employee employee);

    List<Employee> findByName(String employeeName);

    Integer delete(String employeeId);

}
