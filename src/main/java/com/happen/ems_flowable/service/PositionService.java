package com.happen.ems_flowable.service;

import com.happen.ems_flowable.entity.Employee;
import com.happen.ems_flowable.entity.Position;
import org.springframework.web.bind.annotation.PostMapping;

import java.util.List;

/**
 * @program: ems_flowable
 * @description:
 * @author: happen96
 * @create: 2021-04-27 11:03
 **/
public interface PositionService {

    List<Position> list();

    List<Position> findByEmployeeId(Long id);

    Integer add(Position position);

    Integer update(Position position);

    Integer delete(Long id);
}
