package com.happen.ems_flowable.controller;

import com.happen.ems_flowable.common.api.ApiResult;
import com.happen.ems_flowable.entity.Employee;
import com.happen.ems_flowable.service.EmployeeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @program: ems_flowable
 * @description:
 * @author: happen96
 * @create: 2021-04-26 19:52
 **/
@Controller
@RequestMapping("/employee")
public class EmployeeController {
    @Autowired
    private EmployeeService employeeService;

    @GetMapping("/list")
    public ApiResult<List<Employee>> findAllEmployee() {
        return ApiResult.success(employeeService.list());
    }

    @RequestMapping("/findById")
    @ResponseBody
    public ApiResult<Employee> findById(@RequestParam Long id) {
        return ApiResult.success(employeeService.findById(id));
    }

    @PostMapping("/findByName")
    @ResponseBody
    public ApiResult<List<Employee>> findByName(@RequestParam String name) {
        return ApiResult.success(employeeService.findByName(name));
    }


    @PostMapping("/add")
    @ResponseBody
    public ApiResult<Employee> add(@Valid @RequestBody Employee employee) {
        Integer integer = employeeService.add(employee);
        if (integer == 0) {
            return ApiResult.failed("注册失败");
        } else {
            return ApiResult.success(employee);
        }
    }

    @PostMapping("/update")
    @ResponseBody
    public ApiResult<Employee> update(@Valid @RequestBody Employee employee) {
        Integer integer = employeeService.update(employee);
        if (integer == 0) {
            return ApiResult.failed("更新失败");
        } else {
            return ApiResult.success(employee);
        }
    }

    @PostMapping("/delete")
    @ResponseBody
    public ApiResult<String> delete(@RequestParam String id) {
        Integer integer = employeeService.delete(id);
        if (integer == 1) {
            return ApiResult.success("删除成功");
        } else {
            return ApiResult.failed("删除失败");
        }
    }
}
